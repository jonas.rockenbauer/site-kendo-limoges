jQuery(document).ready(function($){

	$('#skip-links a').focus(function() {
		$('#skip-links').addClass('focused');
	}).blur(function() {
		$('#skip-links').removeClass('focused');
	});

    $( ".menu" ).click(function() {
        $(this).toggleClass("actif");
        $('body').toggleClass("win-fixed");
        $("#menu_principal").toggleClass("actif");
    });

    $( "#menu_principal a.twitter" ).empty();
    $( "#menu_principal a.twitter" ).prepend( '<i class="icon-twitter-1"></i>' );

});
